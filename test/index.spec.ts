import { expect } from 'chai';
import * as NewsApi from 'newsapi';

describe("Hello World test", ()=> {
    it("2 + 2 = 4", () => {
        expect(2 + 2).to.be.equal(4);
    });
});

describe("News Api", () => {
    it('', () => {
        const newsapi = new NewsApi('12008d4d327741d2be24e672cc414fdb');
        newsapi.v2.topHeadlines({
            sources: 'bbc-news,the-verge',
            q: 'bitcoin',
            category: 'business',
            language: 'en',
            country: 'us'
          })
          .then(response => { console.log(response); });
    });
});